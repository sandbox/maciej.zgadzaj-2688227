<?php
/**
 * @file
 * Token integration for the Commerce Marketplace Stock Reserve module.
 */

/**
 * Implements hook_tokens_alter().
 *
 * Alters value of 'user-available-stock' token provided
 * by commerce_stock_reserve module.
 *
 * @see commerce_stock_reserve_tokens()
 */
function commerce_marketplace_stock_reserve_tokens_alter(array &$replacements, array $context) {
  if ($context['type'] == 'commerce-product' && !empty($context['data']['commerce-product'])) {
    $product = $context['data']['commerce-product'];
    if (isset($replacements['[commerce-product:user-available-stock]'])) {
      $replacements['[commerce-product:user-available-stock]'] = commerce_marketplace_stock_reserve_get_user_available_amount($product);
    }
  }
}
