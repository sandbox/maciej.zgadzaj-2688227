<?php
/**
 * @file
 * Rules integration for the Commerce Marketplace Stock Reserve module.
 */

/**
 * Implements hook_rules_condition_info_alter().
 *
 * @see commerce_stock_reserve_rules_condition_info()
 */
function commerce_marketplace_stock_reserve_rules_condition_info_alter(&$conditions) {
  if (isset($conditions['commerce_stock_reserve_is_enough_available'])) {
    $conditions['commerce_stock_reserve_is_enough_available']['callbacks']['execute'] = 'commerce_marketplace_stock_reserve_is_enough_available';
  }
}
